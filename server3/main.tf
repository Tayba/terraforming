// Configure the Google Cloud provider
provider "google" {
 credentials = "${file("premium-state-232521-cf8b27962a5e.json")}"
 project     = "premium-state-232521"
 region      = "europe-west3"
}

resource "google_compute_firewall" "default" {
  name    = "firewall-terrafrom-3"
  network = "default"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "22", "8089","3000","9090"]
  }
}

resource "google_compute_disk" "default" {
  name  = "disk3"
  type  = "pd-ssd"
  zone  = "europe-west3-c"
  size = "30"
  labels = {
    environment = "dev"
  }
}

// Terraform plugin for creating random ids
resource "random_id" "instance_id" {
 byte_length = 8
}

data "google_compute_zones" "available" {}

// A single Google Cloud Engine instance
resource "google_compute_instance" "default" {
 name         = "devops-vm3-${random_id.instance_id.hex}"
 machine_type = "n1-standard-2"
 zone         = "europe-west3-c"

 boot_disk {
   initialize_params {
     image = "ubuntu-os-cloud/ubuntu-1604-lts"
   }
 }
 
 metadata_startup_script = "sudo apt-get update\nsudo apt-get install -y docker.io\nsudo usermod -aG docker $USER\n"

 metadata {
   enable-oslogin = "true"
   sshKeys = "bastiba:${file("ssh_public.pub")}"
 }
 network_interface {
   network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
} 



